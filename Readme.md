## Description du projet .

Le projet consiste en une plateforme de e-learning dédiée aux applications que la HighFive développe. Sur cette plateforme, nous offrirons un catalogue d'applications que les utilisateurs pourront explorer. Chaque application sera présentée par une description écrite et une vidéo de démonstration, leur permettant ainsi de se faire une première idée de son fonctionnement.

Lorsqu'un utilisateur sera intéressé par une application spécifique, il aura la possibilité de suivre une formation complète détaillant le fonctionnement approfondi de celle-ci. Pour accéder à cette formation, l'utilisateur devra se connecter en tant qu'entreprise et procéder à l'achat d'une licence, selon les modalités qui seront définies pour chaque application.

Une fois qu'une entreprise aura obtenu une licence pour une application donnée, elle pourra offrir un accès à cette formation à ses employés. Les employés auront alors la possibilité d'avoir des comptes reliés à leurs entreprise et auront alors la possibilité de se former en profondeur sur l'application et d'en maîtriser toutes les fonctionnalités pour une utilisation optimale.

La plateforme permettra également aux utilisateurs de suivre leur progression tout au long des formations. Des indicateurs de progression seront proposés pour aider les utilisateurs à suivre leurs progression.

Cette approche permettra aux utilisateurs de découvrir et d'évaluer les applications à travers des descriptions détaillées et des vidéos explicatives. Ils pourront ensuite approfondir leurs connaissances grâce à des formations complètes, accessibles après l'achat d'une licence par leur entreprise.

Afin d'offrir une expérience utilisateur optimale, l'interface de l'application sera conviviale et intuitive, facilitant ainsi la navigation entre le catalogue d'applications, les formations disponibles, et le suivi de la progression par les utilisateurs.

Pour garantir la sécurité des données et des transactions, des mesures de sécurité robustes seront mises en place pour protéger les informations confidentielles des utilisateurs et des entreprises.

Intégrer un système de notation et de commentaires permettant aux utilisateurs de partager leurs retours et expériences sur les applications et les formations suivies. 

Les utilisateurs pourront ainsi se baser sur les avis pour choisir les applications les mieux adaptées à leurs besoins.


Ce projet vise à combler un besoin croissant en matière de formation sur les applications que nous développons, en offrant une plateforme pratique et complète pour les entreprises et leurs employés, tout en favorisant l'exploration et l'apprentissage pour les utilisateurs individuels.





## Voici une structure de base détaillée pour l'application de e-learning sur les applications  développées.

## Page d'accueil :

- Présentation du projet avec une brève description de l'objectif de l'application.
- Mise en avant des avantages pour les entreprises et les utilisateurs individuels.
- Bouton d'appel à l'action pour inciter les visiteurs à explorer le catalogue d'applications.

## Catalogue des applications :

- Affichage de la liste des applications disponibles avec des vignettes et des titres attrayants.
- Pour chaque application, affichage d'une courte description, des captures d'écran et une vidéo de démonstration.
- Bouton pour accéder à la formation complète de chaque application.

## Page de détail de l'application :

- Informations détaillées sur l'application sélectionnée avec sa description complète et ses fonctionnalités.
- Vidéo explicative plus approfondie sur son utilisation.
- Avis et commentaires des utilisateurs précédents.

## Formation de l'application :

- Modules ou chapitres détaillés expliquant le fonctionnement de l'application.
- Vidéos de formation, tutoriels et présentations pour chaque module.

## Connexion et achat de licence :

- Page de connexion pour les entreprises avec des champs d'identification.
- Possibilité de créer un compte pour les nouvelles entreprises.
- Page d'achat de licence avec les modalités spécifiées pour chaque application.

## Tableau de bord de l'entreprise :

- Vue d'ensemble des licences achetées et de leur utilisation.
- Possibilité d'ajouter ou de supprimer des employés ayant accès à la formation.

## Suivi de progression de l'utilisateur :

- Tableau de bord de l'utilisateur montrant sa progression dans chaque formation.
- Indicateurs de progression, scores des quiz, et évaluations de performance.

## Support et assistance :

- Page de contact pour poser des questions ou signaler des problèmes.
- FAQ pour répondre aux questions fréquemment posées.

## Paiements et facturation :

- Processus de paiement sécurisé pour les achats de licences.
- Historique des transactions et factures disponibles pour les entreprises.


## Analyse et rapports :

- Rapports détaillés sur l'utilisation de l'application et les progrès des employés pour les entreprises.
- Statistiques sur l'utilisation globale de la plateforme pour les administrateurs.
- Politique de confidentialité et conditions d'utilisation :
- Page avec les informations légales concernant la protection des données et les conditions d'utilisation de l'application.


## Structure provisoire de la base de donnée.

## Table "Entreprises" :

- ID : Identifiant unique de l'entreprise (clé primaire)
- Nom : Nom de l'entreprise
- Email : Adresse e-mail de l'entreprise
- MotDePasse : Mot de passe de l'entreprise (peut être stocké de manière sécurisée en utilisant une fonction de hachage)
- Adresse : Adresse de l'entreprise
- Telephone : Numéro de téléphone de l'entreprise

## Table "Utilisateurs" :

- ID : Identifiant unique de l'utilisateur (clé primaire)
- ID_Entreprise : Identifiant de l'entreprise associée (clé étrangère faisant référence à la table "Entreprises")
- Nom : Nom de l'utilisateur
- Email : Adresse e-mail de l'utilisateur
- MotDePasse : Mot de passe de l'utilisateur (peut être stocké de manière sécurisée en utilisant une fonction de hachage)

## Table "Applications" :

- ID : Identifiant unique de l'application (clé primaire)
- Nom : Nom de l'application
- Description : Description courte de l'application
- CaptureEcran : Chemin vers les captures d'écran de l'application (peut être stocké sous forme de lien ou de chemin relatif)
- VideoDemo : Chemin vers la vidéo de démonstration de l'application (peut être stocké sous forme de lien ou de chemin relatif)

## Table "Formations" :

- ID : Identifiant unique de la formation (clé primaire)
- ID_Application : Identifiant de l'application associée (clé étrangère faisant référence à la table "Applications")
- Titre : Titre de la formation
- Description : Description de la formation

## Table "VideosFormation" :

- ID : Identifiant unique de la vidéo de formation (clé primaire)
- ID_Formation : Identifiant de la formation associée (clé étrangère faisant référence à la table "Formations")
- Titre : Titre de la vidéo
- URL : URL de la vidéo (peut être stocké sous forme de lien ou de chemin relatif)
- Duree : Durée de la vidéo (peut être enregistrée en secondes ou en format HH:MM:SS)

## Table "Licences" :

- ID : Identifiant unique de la licence (clé primaire)
- ID_Application : Identifiant de l'application associée (clé étrangère faisant référence à la table "Applications")
- ID_Entreprise : Identifiant de l'entreprise associée (clé étrangère faisant référence à la table "Entreprises")
- DateAchat : Date d'achat de la licence
- DateExpiration : Date d'expiration de la licence (si applicable)

## Table "ProgressionUtilisateur" :

- ID_Utilisateur : Identifiant de l'utilisateur associé (clé étrangère faisant référence à la table "Utilisateurs")
- ID_VideoFormation : Identifiant de la vidéo de formation associée (clé étrangère faisant référence à la table "VideosFormation")
- Progression : Indicateur de progression de l'utilisateur dans la vidéo de formation (peut être un pourcentage ou un indicateur binaire de complétion)

## Table "AvisUtilisateur" :

- ID_Utilisateur : Identifiant de l'utilisateur associé (clé étrangère faisant référence à la table "Utilisateurs")
- ID_Application : Identifiant de l'application associée (clé étrangère faisant référence à la table "Applications")
- Commentaire : Commentaire laissé par l'utilisateur sur l'application
