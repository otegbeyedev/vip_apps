<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progression_utilisateur', function (Blueprint $table) {
            $table->unsignedBigInteger('id_utilisateur');
            $table->foreign('id_utilisateur')->references('id')->on('utilisateurs');
            $table->unsignedBigInteger('id_video_formation');
            $table->foreign('id_video_formation')->references('id')->on('videos_formation');
            $table->integer('progression');
            $table->timestamps();
            $table->primary(['id_utilisateur', 'id_video_formation']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progression_utilisateur');
    }
};
