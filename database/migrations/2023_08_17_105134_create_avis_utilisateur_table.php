<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avis_utilisateur', function (Blueprint $table) {
            $table->unsignedBigInteger('id_utilisateur');
            $table->foreign('id_utilisateur')->references('id')->on('utilisateurs');
            $table->unsignedBigInteger('id_application');
            $table->foreign('id_application')->references('id')->on('applications');
            $table->text('commentaire');
            $table->timestamps();
            $table->primary(['id_utilisateur', 'id_application']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avis_utilisateur');
    }
};
