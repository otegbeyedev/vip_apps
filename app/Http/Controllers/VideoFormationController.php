<?php

namespace App\Http\Controllers;

use App\Models\VideoFormation;
use Illuminate\Http\Request;

class VideoFormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VideoFormation  $videoFormation
     * @return \Illuminate\Http\Response
     */
    public function show(VideoFormation $videoFormation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VideoFormation  $videoFormation
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoFormation $videoFormation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VideoFormation  $videoFormation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VideoFormation $videoFormation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VideoFormation  $videoFormation
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoFormation $videoFormation)
    {
        //
    }
}
