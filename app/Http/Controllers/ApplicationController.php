<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Formation;
use App\Models\VideoFormation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Inertia\Inertia;

class ApplicationController extends Controller
{
    public function index()
    {
        $applications = Application::all();

        return Inertia::render('Applications/Index', [
            'applications' => $applications,
        ]);
    }

    public function create()
    {
        return Inertia::render('Applications/Create');
    }

    public function store(Request $request)
    {
        try {
            // Traitez et stockez les fichiers images
            $Image_name = $_FILES['captureEcran']['name'];
            $Image_path = $_FILES['captureEcran']['tmp_name'];
            $Image_size = $_FILES['captureEcran']['size'];
            $Image_error = $_FILES['captureEcran']['error'];

            $ext = ['jpg', 'jpeg', 'png', 'gif'];
            $max_size = 8000000;

            $ext_Img = pathinfo($Image_name, PATHINFO_EXTENSION);

            if (in_array($ext_Img, $ext)) {
                if ($Image_size <= $max_size) {
                    if ($Image_error == 0) {
                        $file = uniqid("image-", true);
                        $name = $file . "." . $ext_Img;
                        $location = base_path() . "/storage/app/public/images/" . $name;
                        if (move_uploaded_file($Image_path, $location)) {
                            $successMessage = "L'image a été téléchargée avec succès.";
                            $data['captureEcran'] = $name;
                        } else {
                            return response()->json([
                                'successMessage' => "",
                                'errorMessage' => "Erreur lors du téléchargement de l'image."
                            ]);
                        }
                    } else {
                        return response()->json([
                            'successMessage' => "",
                            'errorMessage' => "Erreur lors du téléchargement de l'image : code d'erreur {$Image_error}."
                        ]);
                    }
                } else {
                    return response()->json([
                        'successMessage' => "",
                        'errorMessage' => "L'image dépasse la taille maximale autorisée. 8Mo maximum."
                    ]);
                }
            } else {
                return response()->json([
                    'successMessage' => "",
                    'errorMessage' => "Format d'image non autorisé. Formats jpg, pnp, gif uniquement."
                ]);
            }

            // Traitez et stockez les fichiers vidéo
            $Video_name = $_FILES['videoDemo']['name'];
            $Video_path = $_FILES['videoDemo']['tmp_name'];
            $Video_size = $_FILES['videoDemo']['size'];
            $Video_error = $_FILES['videoDemo']['error'];

            $ext = ['mp4'];
            $max_size = 500000000;

            $ext_Vid = pathinfo($Video_name, PATHINFO_EXTENSION);

            if (in_array($ext_Vid, $ext)) {
                if ($Video_size <= $max_size) {
                    if ($Video_error == 0) {
                        $file = uniqid("video-", true);
                        $name = $file . "." . $ext_Vid;
                        $location = base_path() . "/storage/app/public/videos/" . $name;
                        if (move_uploaded_file($Video_path, $location)) {

                            $successMessage = "La vidéo a été téléchargée avec succès.";
                            $data['videoDemo'] = $name;
                        } else {
                            return response()->json([
                                'successMessage' => "",
                                'errorMessage' => "Erreur lors du téléchargement de la vidéo."
                            ]);
                        }
                    } else {
                        return response()->json([
                            'successMessage' => "",
                            'errorMessage' => "Erreur lors du téléchargement de la vidéo : code d'erreur {$Video_error}."
                        ]);
                    }
                } else {
                    return response()->json([
                        'successMessage' => "",
                        'errorMessage' => "La vidéo dépasse la taille maximale autorisée. 500Mo maximum."
                    ]);
                }
            } else {
                return response()->json([
                    'successMessage' => "",
                    'errorMessage' => "Format vidéo non autorisé. Format mp4 uniquement."
                ]);
            }

            // Créez une nouvelle instance du modèle Application et stockez les données
            $data = [
                'nom' => $_POST["appName"],
                'description' => $_POST["appDescription"],
                'capture_ecran' => isset($data['captureEcran']) ? $data['captureEcran'] : '',
                'video_demo' => isset($data['videoDemo']) ? $data['videoDemo'] : '',
            ];


            Application::create($data);

            return response()->json([
                'successMessage' => 'Application ajouter avec succès',
                'errorMessage' => ''
            ]);
        } catch (\Exception $th) {
            $message = $th->getMessage();
            $messageTraduit = Lang::get($message);
            return response()->json([
                'successMessage' => '',
                'errorMessage' => $messageTraduit
            ]);
        }
    }

    public function show(Application $application, $id)
    {
        $application = $application->findOrFail($id);

        $formation = Formation::find($id);

        $videoFormation = VideoFormation::where('id_formation', $formation->id)
        ->orderBy('ordre', 'asc')
        ->get()
        ->toArray();

        return Inertia::render('Applications/Show', [
            'application' => $application,
            'formation' => $formation,
            'videoFormation' => $videoFormation,
        ]);
    }

    public function edit($id)
    {
        // À compléter
    }

    public function update(Request $request, $id)
    {
        // À compléter
    }

    public function destroy($id)
    {
        // À compléter
    }
}
