<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Formation;
use App\Models\VideoFormation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Inertia\Inertia;

class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Application $application)
    {
        $application = $application->findOrFail($request->id);

        return Inertia::render('Formations/Create', [
            'application' => $application
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $data = [
                'id_application' => $request->application_id,
                'titre_formation' => $request->nomFormation,
                'description_formation' => $request->descriptionFormation,
                'newRow' => $request->newRow
            ];


            $formation = Formation::create([
                'id_application' => $data['id_application'],
                'titre' => $data['titre_formation'],
                'description' => $data['description_formation'],

            ]);


            if ($formation) {
                foreach ($data['newRow'] as $key => $value) {

                    VideoFormation::create([
                        'id_formation' => $formation->id,
                        'titre' => $value["titreVideo"],
                        'url' => $value["urlVideo"],
                        'duree' => $value["dureeVideo"],
                        'ordre' => $value["ordreVideo"],

                    ]);
                }
            };


            return response()->json([
                'successMessage' => 'Formation créer avec succès',
                'errorMessage' => ''
            ]);

        } catch (\Exception $th) {

           $message = $th->getMessage();
            $messageTraduit = Lang::get($message);
            return response()->json([
                'successMessage' => '',
                'errorMessage' => $messageTraduit
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function show(Formation $formation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function edit(Formation $formation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formation $formation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formation $formation)
    {
        //
    }
}
