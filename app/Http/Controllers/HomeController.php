<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function about(){
        return Inertia::render('About');
    }

    public function catalogue(){
        return Inertia::render('Catalogue');
    }
}
