<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoFormation extends Model
{
    use HasFactory;

    protected $table = 'videos_formation';

    protected $fillable = [
        'id_formation',
        'titre',
        'url',
        'duree',
        'ordre',
    ];
}
