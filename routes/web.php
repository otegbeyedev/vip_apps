<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

// Dashboard
Route::get('/admindashboard', [Controller::class, 'index'])->name('admindashboard')->middleware(['auth', 'verified']);

// Les routes normales
Route::get('/about', [HomeController::class, 'about'])->name('about');
Route::get('/catalogue', [HomeController::class, 'catalogue'])->name('catalogue');

// Applications
Route::get('/application/index', [ApplicationController::class, 'index'])->name('application.index')->middleware(['auth', 'verified']);
Route::get('/application/create', [ApplicationController::class, 'create'])->name('application.create')->middleware(['auth', 'verified']);
Route::post('/application/store', [ApplicationController::class, 'store'])->name('application.store')->middleware(['auth', 'verified']);
Route::get('/application/show/{id}', [ApplicationController::class, 'show'])->name('application.show')->middleware(['auth', 'verified']);

// Formations
Route::post('/formation/create', [FormationController::class, 'create'])->name('formation.create')->middleware(['auth', 'verified']);
Route::post('/formation/store', [FormationController::class, 'store'])->name('formation.store')->middleware(['auth', 'verified']);

// // Entreprises
// Route::get('/application/index', [Controller::class, 'index'])->name('application.index')->middleware(['auth', 'verified']);
// Route::get('/application/create', [ApplicationController::class, 'create'])->name('application.create')->middleware(['auth', 'verified']);
// Route::post('/application/store', [ApplicationController::class, 'store'])->name('application.store')->middleware(['auth', 'verified']);

// // Utilisateurs
// Route::get('/application/index', [Controller::class, 'index'])->name('application.index')->middleware(['auth', 'verified']);
// Route::get('/application/create', [ApplicationController::class, 'create'])->name('application.create')->middleware(['auth', 'verified']);
// Route::post('/application/store', [ApplicationController::class, 'store'])->name('application.store')->middleware(['auth', 'verified']);


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
